package io.bitbucket.mefistofeles66.geradordefrases.feature;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void click(View view) throws IOException {

        final TextView textView = findViewById(R.id.frase);

        new AsyncTask<Void,String,String>(){

            // variavel criada para receber o texto da url
            StringBuilder text = new StringBuilder();
            @Override
            protected String doInBackground(Void... params) {
                try {
                    // Cria uma URL
                    URL url = new URL("https://loripsum.net/api/1/short/plaintext");

                    // Ler o texto retornado pelo servidor
                    BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
                    String str;
                    while ((str = in.readLine()) != null) {
                        // str se refere a linha do texto retornado pelo servido
                        // no entanto o uso do append e para concatenar todas as linas
                        text.append(str);
                    }
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return text.toString();
            }
            protected void onPostExecute(String results){
                // result é a resposta recebida pelo doInBackGround()
                // entao a linha abaixo define o textview com o resultado esperado
                textView.setText(results);
            }
        }.execute();
    }
}

